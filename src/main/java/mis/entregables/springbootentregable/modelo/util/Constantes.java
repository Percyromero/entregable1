package mis.entregables.springbootentregable.modelo.util;

public enum Constantes {

    ESTADO_ACTIVO("activo"),
    ESTADO_INACTIVO("inactivo");

    private final String key;

    Constantes(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

}
