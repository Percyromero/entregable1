package mis.entregables.springbootentregable.servicio;

import mis.entregables.springbootentregable.modelo.Campania;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ServicioCampania {

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<String, Campania> campanias = new ConcurrentHashMap<String, Campania>();


    public String agregar(Campania c){

        c.id = String.valueOf(this.secuenciador.incrementAndGet());

        this.campanias.put(c.id, c);

        return c.id;
    }

    public List<Campania> obtenerCampanias(){
       return this.campanias
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }

    public Campania obtenerCampania(String id){

        final Campania c = this.campanias.get(id);

        return c;
    }

    public void reemplazarCampania(String id, Campania c){

        c.id = id;
        //verificar que existe la campaña antes de reemplazar
        this.campanias.put(id, c);
    }

    public void borrarCampania(String id){

        this.campanias.remove(id);
    }

}
