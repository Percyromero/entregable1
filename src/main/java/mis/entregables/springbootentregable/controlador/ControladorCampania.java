package mis.entregables.springbootentregable.controlador;


import jdk.jshell.Snippet;
import mis.entregables.springbootentregable.modelo.Campania;
import mis.entregables.springbootentregable.servicio.ServicioCampania;
import mis.entregables.springbootentregable.modelo.util.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("${api.v1}/campanias/{idCampania}")
public class ControladorCampania {

    @Autowired
    ServicioCampania servicioCampania;

    @GetMapping
    public ResponseEntity<Campania> obtenerCampania(@PathVariable String idCampania){

        try{
            return ResponseEntity.ok(buscarCampaniaPorId(idCampania));
        }catch (RestClientException ex){
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping
    public ResponseEntity<String> reemplazarCampania(@PathVariable (name = "idCampania")String id,
                                   @RequestBody Campania c){
        buscarCampaniaPorId(id);

        if(c.nombre == null || c.nombre.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(c.descripcion == null || c.descripcion.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(c.fechaInicio == null || c.fechaInicio.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(c.fechaFin == null || c.fechaFin.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(c.estado == null || c.estado.trim().isEmpty() ||
                (!c.estado.trim().equals(Constantes.ESTADO_ACTIVO.getKey()) &&
                 !c.estado.trim().equals(Constantes.ESTADO_INACTIVO.getKey())))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        c.id = id;
        this.servicioCampania.reemplazarCampania(id, c);
        return new ResponseEntity<>("Campaña actualizada correctamente.", HttpStatus.OK);
    }

    @DeleteMapping
    public void borrarCampania(@PathVariable String idCampania){
        this.servicioCampania.borrarCampania(idCampania);
    }

    @PatchMapping
    public ResponseEntity<String>  modificarCompania(@PathVariable String idCampania,
                                  @RequestBody Campania c)  {

        final Campania resultC = buscarCampaniaPorId(idCampania);

        if(c.nombre != null ){
            if(c.nombre.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            resultC.nombre = c.nombre;
        }

        if(c.descripcion != null){
            if(c.descripcion.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            resultC.descripcion = c.descripcion;
        }

        if(c.fechaInicio != null){
            if(c.fechaInicio.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            resultC.fechaInicio = c.fechaInicio;
        }

        if(c.fechaFin != null){
            if(c.fechaFin.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            resultC.fechaFin = c.fechaFin;
        }

        if(c.estado != null){
            if(c.estado.trim().isEmpty() || (!c.estado.trim().equals(Constantes.ESTADO_ACTIVO.getKey()) &&
                    !c.estado.trim().equals(Constantes.ESTADO_INACTIVO.getKey())))
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            resultC.estado = c.estado;
        }

        this.servicioCampania.reemplazarCampania(idCampania, resultC);
        return new ResponseEntity<>("Campaña modificada correctamente.", HttpStatus.OK);
    }

    private Campania buscarCampaniaPorId(String idCampania){
        final Campania c = this.servicioCampania.obtenerCampania(idCampania);
        if(c == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return c;
    }

}
