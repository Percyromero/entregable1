package mis.entregables.springbootentregable.controlador;

import mis.entregables.springbootentregable.modelo.Campania;
import mis.entregables.springbootentregable.servicio.ServicioCampania;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("${api.v1}/campanias")
public class ControladorListaCampanias {

    @Autowired
    ServicioCampania servicioCampania;

    //POST
    @PostMapping
    public ResponseEntity<String> agregarCampania(@RequestBody Campania c){

        this.servicioCampania.agregar(c);
        return new ResponseEntity<>("Campaña creada existosamente!", HttpStatus.CREATED);
    }

    //GET
    @GetMapping
    public List<Campania> obtenerCampanias(){
        return this.servicioCampania.obtenerCampanias();
    }

}
